const gulp = require("gulp");
const clean = require("gulp-clean");
const connect = require("gulp-connect");
const sass = require('gulp-sass');
const minifyJS = require('gulp-minify');
const pkg = require("./package.json");

const srcDir = pkg.project.srcDir;
const distDir = pkg.project.distDir;
const publicDir = pkg.project.publicDir;

gulp.task("clean", function() {
  return gulp.src([distDir + "/*"], {
    read: false
  })
  .pipe(clean());
});

gulp.task("reload", function() {
  return gulp.src([distDir + "/**/*", publicDir + "/**/*"])
  .pipe(connect.reload());
});

gulp.task("styles", function() {
  return gulp.src([srcDir + "/scss/**/*.scss"])
  .pipe(sass({
    includePaths: ["node_modules/foundation-sites/scss"]
  }).on('error', sass.logError))
  .pipe(gulp.dest(publicDir));
});

gulp.task("minify", function() {
  return gulp.src([srcDir + "/main/**/*.js"])
  .pipe(minifyJS())
  .pipe(gulp.dest(distDir));
});

gulp.task("serve", ["styles"], function() {
  connect.server({
    root: [srcDir + "/main", publicDir],
    port: 3000,
    livereload: true
  })
});

gulp.task("watch", ["serve"], function () {
  gulp.watch([srcDir + "/main/**/*", publicDir + "/**/*"], ["reload"]);
  gulp.watch([srcDir + "/scss/**/*"], ["styles"]);
});

gulp.task("build", ["minify", "styles"]);

gulp.task("default", ["build", "watch"]);
